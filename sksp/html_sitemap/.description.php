<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); 
if(!CModule::IncludeModule("iblock"))
    return;
$arComponentDescription = array(
    "NAME" => GetMessage("SKSP_SMP_COMPONENT_NAME"),
    "DESCRIPTION" => GetMessage("SKSP_SMP_COMPONENT_DESCRIPTION"),
    "PATH" => array(
        'NAME' => GetMessage("SKSP_SMP_PATH_NAME"),
        "ID" => "skspcompany",
        "CHILD" => array(
            "ID" => "htmlsitemapsksp",
            "NAME" => GetMessage("SKSP_SMP_PATH_CHILD_NAME"),
        ),
    ),
);
?>